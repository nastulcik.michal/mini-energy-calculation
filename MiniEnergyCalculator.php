<?php 

/**
 * Calculator for calculation energy price from different providers
 */
class MiniEnergyCalculator
{
	protected $formTemplate = [
		'commodity' => ["electro","gas"],
		'price' => 'int',
		'mwh' => "float",
		'opm' => "int"
	];

	protected $tarifs = [
		'electro' => [
			'tarif1' => 3.9000,
			'tarif2' => 20.9999,
			'tarif3' => 20.9999,
		],
		'gas' => [
			'tarif1' => 5.9999,
			'tarif2' => 25.9999,
			'tarif3' => 50.0000,
		],
	];

	protected $providers = [
		'provider1' => [
			'name' => 'Nejlevnější energie',
			'electro' => 
			[
				'tarif1' => [
					'price' => 1200,
					'tax' => 100,
				],
				'tarif2' => [
					'price' => 1000,
					'tax' => 80,
				],
				'tarif3' => [
					'price' => 1000,
					'tax' => 80,
				],
			],
			'gas' => 
			[
				'tarif1' => [
					'price' => 900,
					'tax' => 100,
				],
				'tarif2' => [
					'price' => 850,
					'tax' => 90,
				],
				'tarif3' => [
					'price' => 750,
					'tax' => 80,
				],
			],
		],
		'provider2' => [
			'name' => 'Šetříme všem',
			'electro' => 
			[
				'tarif1' => [
					'price' => 1300,
					'tax' => 110,
				],
				'tarif2' => [
					'price' => 900,
					'tax' => 70,
				],
				'tarif3' => [
					'price' => 800,
					'tax' => 60,
				],
			],
			'gas' => 
			[
				'tarif1' => [
					'price' => 930,
					'tax' => 110,
				],
				'tarif2' => [
					'price' => 800,
					'tax' => 80,
				],
				'tarif3' => [
					'price' => 730,
					'tax' => 50,
				],
			],
		],
		'provider3' => [
			'name' => 'Běžný dodavatel',
			'electro' => 
			[
				'tarif1' => [
					'price' => 1100,
					'tax' => 80,
				],
				'tarif2' => [
					'price' => 1050,
					'tax' => 50,
				],
				'tarif3' => [
					'price' => 900,
					'tax' => 80,
				],
			],
			'gas' => 
			[
				'tarif1' => [
					'price' => 920,
					'tax' => 110,
				],
				'tarif2' => [
					'price' => 800,
					'tax' => 80,
				],
				'tarif3' => [
					'price' => 800,
					'tax' => 50,
				],
			],
		]
	];

	private $request;
	private $customerData;

	public $providersData = [];
	public $errorMessage = false;
	
	function __construct()
	{
		$this->request = $this->controlGetData();
		if (!$this->errorMessage) {
			$this->calculation();
		}
	}

	private function controlGetData()
	{
		$rawRequest = array_filter($_GET);
		if (!$this->controlRequestCount($rawRequest)) {
			return false;
		}
		if (!$this->controlRequestKeys($rawRequest)) {
			return false;
		}
		if (!$this->controlRequestValues($rawRequest)) {
			return false;
		}

		$rawRequest = $this->reformatValues($rawRequest);

		if (!$this->controlTarifValue($rawRequest)) {
			return false;
		}
		return $rawRequest;
	}

	public function controlTarifValue($rawRequest)
	{
		if ($rawRequest['mwh'] > 50) {
			$maximalValue = end($this->tarifs[$rawRequest['commodity']]);
			$this->errorMessage = 'Hodnota v "Roční spotřeba" je příliš vysoká. Tato hodnota může být maximálně '.$maximalValue.' MWh.';
			return false;	
		}

		if ($rawRequest['mwh'] < $this->tarifs[$rawRequest['commodity']]['tarif1']) {
			$this->errorMessage = 'Hodnota v "Roční spotřeba" je příliš nízká. Tato hodnota musí být vyšší než '. $this->tarifs[$rawRequest['commodity']]['tarif1'] .' MWh.';
			return false;	
		}
		return true;
	}

	public function reformatValues($rawRequest)
	{
		$rawRequest['mwh'] = (float) preg_replace("/[^0-9.]/", "", $rawRequest['mwh']);
		$rawRequest['opm'] = (int) preg_replace("/[^0-9.]/", "",$rawRequest['opm']);
		$rawRequest['price'] = (int) preg_replace("/[^0-9.]/", "",$rawRequest['price']);
		return $rawRequest;
	}

	private function controlRequestCount($rawRequest)
	{
		if (count($rawRequest) != 4) {
			$this->errorMessage = 'Bohužel nebyly nastaveny potřebné hodnoty pro výpočet, zkuste to prosím znovu.';
			return false;
		}
		return true;
	}

	/**
	 * Control if request array contain all keys by template - $this->formKeys 
	 */
	private function controlRequestKeys($rawRequest)
	{
		if (array_keys($this->formTemplate) != array_keys($rawRequest)) {
			$this->errorMessage = 'Bohužel byly nastaveny špatné hodnoty pro výpočet, zkuste to prosím znovu.';
			return false;
		}
		return true;
	}

	private function controlRequestValues($rawRequest)
	{
		if (!in_array($rawRequest['commodity'], $this->formTemplate['commodity'])) {
			$this->errorMessage = 'Pro komoditu "' . $rawRequest['commodity'] . '" bohužel nemáme kalkulačku :-/';
			return false;	
		}

		if (!is_numeric($rawRequest['price'])) {
			$this->errorMessage = 'Hodnota v "Cena" musí být číselná.';
			return false;	
		}

		if (!is_numeric($rawRequest['mwh'])) {
			$this->errorMessage = 'Hodnota v "Roční spotřeba" musí být číselná.';
			return false;	
		}

		if (!is_numeric($rawRequest['opm'])) {
			$this->errorMessage = 'Hodnota v "Poplatek za OPM" musí být číselná.';
			return false;	
		}
		return true;
	}

	private function calculation()
	{
		if ($this->request['commodity'] == "electro") {
			$this->getProvidersPrice('electro', $this->getTarif("electro", $this->request['mwh']));
		}
		if ($this->request['commodity'] == "gas") {
			$this->getProvidersPrice('gas', $this->getTarif('gas', $this->request['mwh']));
		}
	}

	private function getProvidersPrice($commodityType, $tarif)
	{
		$this->initCustomerData();
		$this->providersData['Můj dodavatel'] = $this->customerData;

		foreach ($this->providers as $provider => $providerData) {
			$commodityTarifs = $providerData[$commodityType][$tarif];
			$this->providersData[$providerData['name']] = $this->getProviderData($commodityTarifs, $providerData);
		}
		asort($this->providersData);
	}


	private function getProviderData($commodityTarifs, $providerData)
	{
		$provider = [];
		$price =  $this->calculatePrice($this->request['mwh'], $commodityTarifs['price'], $commodityTarifs['tax']);
		$priceDiff = $this->customerData['price'] - $price;

		$provider['price'] = $price;
		$provider['price_diff'] = ($priceDiff > 0) ?  "+".$priceDiff : $priceDiff;
		$provider['tr_color'] = ($priceDiff > 0) ? "green" : "red";
		return $provider;
	}

	// $spotřeba * $cena + 12 * $poplatek
	private function calculatePrice($mwh, $price, $tax)
	{
		return $mwh * $price + 12 * $tax;	
	}

	private function initCustomerData()
	{
		$this->customerData = [
			'price' => $this->calculatePrice($this->request['mwh'], $this->request['price'], $this->request['opm']),
			"price_diff" => 0,
			'tr_color' => "light",
		];
	}

	private function getTarif($energyType, $consumption)
	{

		foreach ($this->tarifs[$energyType] as $tarif => $value) {
			if ($consumption >= $value) {
				return $tarif;
			}
		}
		return $tarif;
	}
}